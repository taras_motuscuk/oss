<?php include('header.php'); ?>
        <div id="left-column">
	    <?php if($user->uid) { ?>
	    <div class="admin-menu">
		<div class="title-block">admin menu</div>
		<?php print theme('links', menu_navigation_links('navigation')); ?>
	    </div>
	    <?php } ?>
            <div class="filter">
                <div class="title-block">каталог осушителей</div>
                <div class="product-filter">
                    <div class="item">
                        <div class="section first">
                            <div class="text">Для бассейнов</div>
                        </div>
                        <ul>
                            <?php
			    $tree = taxonomy_get_tree(1,0,-1,1);
			    foreach($tree as $key => $term){
			    echo '<li>'.l($term->name,'taxonomy/term/'.$term->tid).'</li>';
			    }
			    ?>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="section">
                            <div class="text">Промышленные</div>
                        </div>
                        <ul>
                            <?php
			    $tree = taxonomy_get_tree(2,0,-1,1);
			    foreach($tree as $key => $term){
			    echo '<li>'.l($term->name,'taxonomy/term/'.$term->tid).'</li>';
			    }
			    ?>
                        </ul>
                    </div>
                    <div class="item">
                        <div class="section">
                            <div class="text">Все производители</div>
                        </div>
                        <ul>
                           <?php
			    $tree = taxonomy_get_tree(3,0,-1,1);
			    foreach($tree as $key => $term){
			    echo '<li>'.l($term->name,'taxonomy/term/'.$term->tid).'</li>';
			    }
			    ?>
                        </ul>
                    </div>  
                </div>
            </div>
            <div class="ban">
                <div class="inner">
                    <?php print $firstbanner; ?>
                </div>
            </div>
                <div class="poll-block">
                <div class="title-block">опрос</div>
		<?php print $poll; ?>
            </div>
        </div>
	<div id="content">
	    <div id="tab">
	    <ul class="tabs">
                <?php
		$tree = taxonomy_get_tree(6,0,-1,1);
		foreach($tree as $key => $term){
		echo ''.l($term->name,'taxonomy/term/'.$term->tid).'';
		}
		?>
            </ul>
	    </div>
	    <?php print $breadcrumds; ?>
		<h1><?php print $title; ?></h1>
	    
	    <div class="product">
		<?php print $content; ?>
		<div class="fullproduct">
		    <div class="item">
			<div class="title-block">Danterm</div>
			<div class="img-box">
			    <a href="/sites/default/files/300-1000.png" rel="magnify"><img src="/sites/default/files/300-1000.png" alt="" width="430" height="400"></a>
			    <div class="action"><span>Акция</span></div>
			    <div class="novelty"><span>Новинка</span></div>
			</div>
			<div class="about-product">
			    <div class="manufacturer">Производитель:<span>Danterm</span></div>
			    <div class="product-model">Модель:<span>400</span></div>
			    <h5>Общие параметры:</h5>
				<ul class="parametr-list">
				    <li>
					<a href="#" class="instr">Скачать инструкцию</a>
				    </li>
				     <li>
					<a href="#" class="services-install">Потребуются услуги монтажа</a>
				    </li>
				    <li>
					<a href="#" class="design-services">Потребуются услуги проектирования</a>
				    </li>
				    <li>
					<a href="#" class="dehumidifier">Проверить - подходит-ли осушитель</a>
				    </li>
				    <li>
					<a href="#" class="delivery-time">Срок поставки<span>2 недели</span></a>
				    </li>
				</ul>
			</div>
			<div class="product-by">
			    <div class="price">
				<div class="last_price">1000$</div>
				<div class="new_price">2000$</div>
				<a href="#" class="by">Купить</a>
				<div class="qty">В наличии</div>
			    </div>
			</div>
			<div class="description-content">
			    <h2>описание и применение:</h2>
			    <p>Стационарный осушитель Dantherm CDP 35 – идеальное выбор для установки в плавательных бассейнах.
			    Роторный компрессор и радиальный вентилятор обеспечивают качественную работу.
			    Материал корпуса- горячеоцинкованная сталь. Для большей защиты, корпус покрыт
			    внутри и снаружи слоем эмали. Комплект поставки предусматривает кронштейны для
			    настенного монтажа. Если есть необходимость напольного крепления возможно
			    приобретение дополнительного комплекта. Отверстие для слива расположено внизу
			    осушителя Dantherm CDP 35.</p>
			    <div class="text">Инновации моделей DH 44/DH 66</div>
			    <ul>
				<li>
				    <span class=list>Значительно более низкий уровень шума</span>
				</li>
				<li>
				    <span class=list>Самое низкое энергопотребление, по сравнению с другими моделями аналогичной производительности</span>
				</li>
				<li>
				    <span class=list>Универсальное крепление для напольного и настенного монтажа осушителей</span>
				</li>
				<li>
				    <span class=list>Возможность выбора направления подачи сухого воздуха (горизонтального/вертикального)</span>
				</li>
				<li>
				    <span class=list>Теплообменники покрыты антикоррозийным покрытием</span>
				</li>
				<li>
				    <span class=list>Оснащены всеми функциями, что и стандартные модели</span>
				</li>
			    </ul>
			    <div class="text">Конструктивные и функциональные особенности</div>
			    <ul>
				<li>
				    <span class=list>Значительно более низкий уровень шума</span>
				</li>
				<li>
				    <span class=list>Самое низкое энергопотребление, по сравнению с другими моделями аналогичной производительности</span>
				</li>
				<li>
				    <span class=list>Универсальное крепление для напольного и настенного монтажа осушителей</span>
				</li>
				<li>
				    <span class=list>Возможность выбора направления подачи сухого воздуха (горизонтального/вертикального)</span>
				</li>
				<li>
				    <span class=list>Теплообменники покрыты антикоррозийным покрытием</span>
				</li>
				<li>
				    <span class=list>Оснащены всеми функциями, что и стандартные модели</span>
				</li>
			    </ul>
			    <div class="text">Дополнительные опции</div>
			    <ul>
				<li>
				    <span class=list>Возможность выбора направления подачи сухого воздуха (горизонтального/вертикального)</span>
				</li>
				<li>
				    <span class=list>Теплообменники покрыты антикоррозийным покрытием</span>
				</li>
				<li>
				    <span class=list>Оснащены всеми функциями, что и стандартные модели</span>
				</li>
			    </ul>
			    <div class="text" style="text-decoration: underline;">Применение</div>
			    <p>Бассейны, СПА, сауны, комнаты для переодевания, душевые</p>
			</div>
			<div class="technical">
			    <h2>технические характеристики</h2>
			    <div class="tech-content">
				<ul>
				    <li>
					Рабочий диап. влажность (% RH)<span>20-100%</span>
				    </li>
				    <li>
					Рабочий диап. температур (°C)<span>15-35°</span>
				    </li>
				    <li>
					Разход воздуха м.куб./час<span>740</span>
				    </li>
				    <li>
					Параметры эл.питания (В/Гц)<span>230/50</span>
				    </li>
				    <li>
					Макс. потребляемый ток (А)<span>4,2</span>
				    </li>
				    <li>
					Макс. потребляемая мощность (кВт)<span>0,980</span>
				    </li>
				    <li>
					Хладагент<span>R407C</span>
				    </li>
				    <li>
					Количество хладагента (кг)<span>1,700</span>
				    </li>
				    <li>
					Уровень шума (дБ)<span>44</span>
				    </li>
				    <li>
					Вес (кг)<span>74</span>
				    </li>
				    <li>
					Класс защиты<span>IP45</span>
				    </li>
				    <li>
					Высота (мм)<span>1026</span>
				    </li>
				    <li>
					Ширина (мм)<span>1345</span>
				    </li>
				    <li>
					Глубина (мм)<span>340</span>
				    </li>
				</ul>
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	    <div class="clear"></div>
	    <div class="title-block">Отзывы:</div>
	    <div class="comment">
		<h3>Максим Максимович:</h3>
		<p>Брал для спортзала, так как полуподвал со всеми вытекающими последствиями (сырость значительная).
		Плошадь примерно квадратов 35 и могу отметить что уже через пол-дня работы воздух стал суше.
		За это время осушитель набрал почти полный бак воды. Сложности в том чтобы разобраться не было -
		просто внимательно прочёл инструкцию, хотя там и без неё понятно практически всё.
		Приятно, что гарантия 3 года и сервисный центр поддерживает гарантию весь этот срок (специально звонил-уточнял).</p>
	    </div>
	    <div class="comment-form">
		<div class="form-item" id="edit-name-wrapper">
		    <label for="edit-name">Имя:<span class="form-required" title="Обязательно для заполнения.">*</span></label>
		    <input type="text" maxlength="255" name="name" id="edit-name" size="60" value="" class="form-text required">
		</div>
		<div class="form-item" id="edit-mail-wrapper">
		    <label for="edit-mail">Е-mail: <span class="form-required" title="Обязательно для заполнения.">*</span></label>
		    <input type="text" maxlength="255" name="mail" id="edit-mail" size="60" value="" class="form-text required">
		</div>
		<div class="form-item" id="edit-message-wrapper">
		    <label for="edit-message">Текст сообщения: <span class="form-required" title="Обязательно для заполнения.">*</span></label>
		    <div class="resizable-textarea"><span><textarea cols="60" rows="5" name="message" id="edit-message" class="form-textarea resizable required textarea-processed"></textarea></span></div>
		</div>
		<input type="submit" name="op" id="edit-submit" value="Отправить" class="form-submit">
	    </div>
	    <div class="title-block">модели, похожие на dantherm ad 400 b:</div>
	    <div class="like-model">
		<?php print $temp; ?>
		<div class="item">
                    <div class="box">
                       <a href="http://osushiteli.prodaj.com.ua/sites/default/files/cdt60.png" title="Dantherm CDT 40 S" class="colorbox imagefield imagefield-imagelink imagefield-field_image_cache initColorbox-processed cboxElement" rel="gallery-35"><img src="http://osushiteli.prodaj.com.ua/sites/default/files/imagecache/product/cdt60.png" alt="" title="Dantherm CDT 40 S" width="180" height="160" class="imagecache imagecache-product"></a>
                    </div>
		    <h3>Dantherm CDT 40 S</h3>
                    <div class="description">
                        <p>Мобильный осушитель воздуха с двумя...
                    </p></div>
                    <div class="price">
                        <div class="last_price"><span class="uc-price-product uc-price-list_price uc-price">€0.00</span></div>
                        <div class="new_price"><span class="uc-price-product uc-price-sell_price uc-price">€0.00</span></div>
                    </div>
                    <form action="/" accept-charset="UTF-8" method="post" id="uc-catalog-buy-it-now-form-35" class="ajax-cart-submit-form uc-out-stock-processed">
		    <div><input type="hidden" name="nid" id="edit-nid-9" value="35">
		    <input type="submit" name="op" id="edit-submit-35" value="Купить" class="form-submit list-add-to-cart ajax-cart-submit-form-button ajax-cart-processed" style=""><div class="uc_out_of_stock_html"></div>
		    <input type="hidden" name="form_build_id" id="form-6d61c6c5dd64b307c7538e2be922686a" value="form-6d61c6c5dd64b307c7538e2be922686a">
		    <input type="hidden" name="form_token" id="edit-uc-catalog-buy-it-now-form-35-form-token" value="c88fe221bc381ea039be08f7bea96d55">
		    <input type="hidden" name="form_id" id="edit-uc-catalog-buy-it-now-form-35" value="uc_catalog_buy_it_now_form_35">
		    <input type="hidden" name="product-nid" id="edit-product-nid-9" value="35">
		    </div>
		    </form>
                    <div class="qty">В наличии</div>
		</div>
		<div class="item">
                    <div class="box">
                       <a href="http://osushiteli.prodaj.com.ua/sites/default/files/ch-008-014.png" title="Cooper&amp;Hunter CH-D008WM" class="colorbox imagefield imagefield-imagelink imagefield-field_image_cache initColorbox-processed cboxElement" rel="gallery-32"><img src="http://osushiteli.prodaj.com.ua/sites/default/files/imagecache/product/ch-008-014.png" alt="" title="Cooper&amp;Hunter CH-D008WM" width="180" height="160" class="imagecache imagecache-product"></a>
                    </div>
		    <h3>Dantherm CDT 40 S</h3>
                    <div class="description">
                        <p>Мобильный осушитель воздуха с двумя...
                    </p></div>
                    <div class="price">
                        <div class="last_price"><span class="uc-price-product uc-price-list_price uc-price">€0.00</span></div>
                        <div class="new_price"><span class="uc-price-product uc-price-sell_price uc-price">€0.00</span></div>
                    </div>
                    <form action="/" accept-charset="UTF-8" method="post" id="uc-catalog-buy-it-now-form-35" class="ajax-cart-submit-form uc-out-stock-processed">
		    <div><input type="hidden" name="nid" id="edit-nid-9" value="35">
		    <input type="submit" name="op" id="edit-submit-35" value="Купить" class="form-submit list-add-to-cart ajax-cart-submit-form-button ajax-cart-processed" style=""><div class="uc_out_of_stock_html"></div>
		    <input type="hidden" name="form_build_id" id="form-6d61c6c5dd64b307c7538e2be922686a" value="form-6d61c6c5dd64b307c7538e2be922686a">
		    <input type="hidden" name="form_token" id="edit-uc-catalog-buy-it-now-form-35-form-token" value="c88fe221bc381ea039be08f7bea96d55">
		    <input type="hidden" name="form_id" id="edit-uc-catalog-buy-it-now-form-35" value="uc_catalog_buy_it_now_form_35">
		    <input type="hidden" name="product-nid" id="edit-product-nid-9" value="35">
		    </div>
		    </form>
                    <div class="qty">В наличии</div>
		</div>
		<div class="item">
                    <div class="box">
                       <a href="http://osushiteli.prodaj.com.ua/sites/default/files/cdp35.png" title="Dantherm CDP 35" class="colorbox imagefield imagefield-imagelink imagefield-field_image_cache initColorbox-processed cboxElement" rel="gallery-34"><img src="http://osushiteli.prodaj.com.ua/sites/default/files/imagecache/product/cdp35.png" alt="" title="Dantherm CDP 35" width="180" height="160" class="imagecache imagecache-product"></a>
                    </div>
		    <h3>Dantherm CDT 40 S</h3>
                    <div class="description">
                        <p>Мобильный осушитель воздуха с двумя...
                    </p></div>
                    <div class="price">
                        <div class="last_price"><span class="uc-price-product uc-price-list_price uc-price">€0.00</span></div>
                        <div class="new_price"><span class="uc-price-product uc-price-sell_price uc-price">€0.00</span></div>
                    </div>
                    <form action="/" accept-charset="UTF-8" method="post" id="uc-catalog-buy-it-now-form-35" class="ajax-cart-submit-form uc-out-stock-processed">
		    <div><input type="hidden" name="nid" id="edit-nid-9" value="35">
		    <input type="submit" name="op" id="edit-submit-35" value="Купить" class="form-submit list-add-to-cart ajax-cart-submit-form-button ajax-cart-processed" style=""><div class="uc_out_of_stock_html"></div>
		    <input type="hidden" name="form_build_id" id="form-6d61c6c5dd64b307c7538e2be922686a" value="form-6d61c6c5dd64b307c7538e2be922686a">
		    <input type="hidden" name="form_token" id="edit-uc-catalog-buy-it-now-form-35-form-token" value="c88fe221bc381ea039be08f7bea96d55">
		    <input type="hidden" name="form_id" id="edit-uc-catalog-buy-it-now-form-35" value="uc_catalog_buy_it_now_form_35">
		    <input type="hidden" name="product-nid" id="edit-product-nid-9" value="35">
		    </div>
		    </form>
                    <div class="qty">В наличии</div>
		</div>
	    </div>
	</div>
        <?php include('footer.php'); ?>