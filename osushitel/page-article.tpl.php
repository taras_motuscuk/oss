<?php include('header.php'); ?>  
  <div class="inner-blocks margin-inner">
    <div class="wrapper-inner">
      <div class="node-tabs">
	 <ul>
	   <li><a href="/article" class="active">Блог</a></li>
	   <li><a href="/news">Новости</a></li>
	   <li><a href="/portfolio">Реализованные проекты</a></li>
	 </ul>
      </div>      
      <?php echo views_embed_view('lists','default', 'all', 'useful_article');?>
    </div>
  </div>
<?php include('footer.php'); ?>
