<div class="front_vid">
    <a class="youtube colorbox-load" href="https://www.youtube.com/embed/<?php print $fields['field_video_value']->content; ?>?width=724&height=424&iframe=true&autoplay=1&rel=0&wmode=transparent" title="<?php print $fields['field_video_name_value']->content; ?><?php echo htmlspecialchars('<a href="'.$fields['path']->content.'" class="colorbox-button">Посмотреть осушитель</a>', ENT_QUOTES, "UTF-8"); ?>">
        <span class="video-title"><?php print $fields['field_video_name_value']->content; ?></span>
        <span class="video-screenshot">
            <img class="group1 youtubeplay" src="/sites/default/files/youtube/<?php print $fields['field_video_value']->content; ?>.jpg" alt="<?php print $fields['field_video_name_value']->content; ?>"/><span></span>
        </span>
    </a>
</div>
