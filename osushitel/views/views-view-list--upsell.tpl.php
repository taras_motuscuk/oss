<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>
 

 <?php
			$i=1;
foreach ($rows as $id => $row):
if($i==1) echo '<span>';
print $row;
if($i==3) {
echo '</span>';
$i=0;
}
$i++;
endforeach;?>