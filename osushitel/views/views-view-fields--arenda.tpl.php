<div class="rent-row">
				<div class="rent-title"><?php print $fields['title']->content; ?></div>
				<div class="rent-box">
								<div class="rent-box-left">
												<?php print $fields['field_image_cache_fid']->content; ?>
								</div>
								<div class="rent-box-right">
												<h5>Параметры:</h5>
												<ul class="parametr-list">
				
				<?php  if($fields['field_apartments_up_value']->content!=''): ?>
				   <li>
					Для квартир площадью до:<span><?php print $fields['field_apartments_up_value']->content; ?></span>
				   </li>
			      <?php endif; ?>
						
						<?php  if($fields['field_area_value']->content!=''): ?>
				   <li>
					Для бассейнов с площадью до:<span><?php print $fields['field_area_value']->content; ?></span>
				   </li>
			      <?php endif; ?>
						
						<?php  if($fields['field_rooms_value']->content!=''): ?>
				   <li>
					Для помещений объемом до:<span><?php print $fields['field_rooms_value']->content; ?></span>
				   </li>
			      <?php endif; ?>
			      <?php  if($fields['field_performance_value']->content!=''): ?>
				   <li>
					Производительность при 30°/80%:<span><?php print $fields['field_performance_value']->content; ?> л/сут</span>
				   </li>
			      <?php endif; ?>
						
						
			      <?php  if($fields['field_principle_value']->content!=''): ?>
				   <li>
					Диапазон работы гидростата:<span><?php print $fields['field_principle_value']->content; ?> %</span>
				   </li>
			      <?php endif; ?>
			      <?php  if($fields['field_range_value']->content!=''): ?>
				   <li>
					Диапазон рабочих температур:<span><?php print $fields['field_range_value']->content; ?> °C</span>
				   </li>
			      <?php endif; ?>
			      <?php  if($fields['field_size_value']->content!=''): ?>
				   <li>
					Размеры (в/ш/г):<span><?php print $fields['field_size_value']->content; ?> мм</span>
				   </li>
			      <?php endif; ?>
			      <?php  if($fields['field_weight_value']->content!=''): ?>
				   <li>
					Вес:<span><?php print $fields['field_weight_value']->content; ?> кг</span>
				   </li>
			      <?php endif; ?>
			</ul>
												<div class="order-row">
																<a href="#" rel="<?php print $fields['nid']->content; ?>">Заказать</a>
																<div class="rent-price"><?php print $fields['field_rent_price_value']->content; ?></div>
												</div>
								</div>
				</div>
				<div style="display: none;">
				<div class="buy_form" id="buy_form_<?php print $fields['nid']->content; ?>">
    
    <div class="buy_head">
      <h2>Аренда осушителя <?php print $fields['title']->content; ?></h2>
    </div>
    <?php
    $buy = drupal_get_form('shop_buy_form', $fields['nid']->content);
    print $buy;
    ?>
</div>
				</div>
</div>