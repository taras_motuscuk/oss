<div class="item">		    
  <div class="img-box">
    <a href="<?php print $fields['path']->content; ?>"><?php print $fields['field_image_cache_fid']->content; ?></a>
    <a href="<?php print $fields['path']->content; ?>"><span class="action"<?php print $fields['field_action_product_value']->content; ?>><span>Акция</span></span></a>
    <a href="<?php print $fields['path']->content; ?>"><span class="novelty"<?php print $fields['field_novelty_product_value']->content; ?>><span>Хит продаж</span></span></a>
  </div>
  <div class="about-product">
    <div class="title-block"><?php print $fields['title']->content; ?></div>
      <?php if($user->uid==1): ?>
	<div class="product-model" style="position: relative; top:-6px;">Категория:<span><?php print GetCategoryByNid($fields['nid']->content); ?></span></div>
      <?php endif; ?>
      <div class="product-model">Модель:<span><?php print $fields['field_model_value']->content; ?></span></div>
      <div class="type">Тип осушителя:<span><?php print $fields['field_type_value']->content; ?></span></div>
			<ul class="parametr-list">
				
				<?php  if($fields['field_apartments_up_value']->content!=''): ?>
				   <li>
					Для квартир площадью до:<span><?php print $fields['field_apartments_up_value']->content; ?></span>
				   </li>
			      <?php endif; ?>
						
						<?php  if($fields['field_area_value']->content!=''): ?>
				   <li>
					Для бассейнов с площадью до:<span><?php print $fields['field_area_value']->content; ?></span>
				   </li>
			      <?php endif; ?>
						
						<?php  if($fields['field_rooms_value']->content!=''): ?>
				   <li>
					Для помещений объемом до:<span><?php print $fields['field_rooms_value']->content; ?></span>
				   </li>
			      <?php endif; ?>
			      <?php  if($fields['field_performance_value']->content!=''): ?>
				   <li>
					Производительность при 30°/80%:<span><?php print $fields['field_performance_value']->content; ?> л/сут</span>
				   </li>
			      <?php endif; ?>
						
						
			      <?php  if($fields['field_principle_value']->content!=''): ?>
				   <li>
					Диапазон работы гидростата:<span><?php print $fields['field_principle_value']->content; ?> %</span>
				   </li>
			      <?php endif; ?>
			      <?php  if($fields['field_range_value']->content!=''): ?>
				   <li>
					Диапазон рабочих температур:<span><?php print $fields['field_range_value']->content; ?> °C</span>
				   </li>
			      <?php endif; ?>
			      <?php  if($fields['field_size_value']->content!=''): ?>
				   <li>
					Размеры (в/ш/г):<span><?php print $fields['field_size_value']->content; ?> мм</span>
				   </li>
			      <?php endif; ?>
			      <?php  if($fields['field_weight_value']->content!=''): ?>
				   <li>
					Вес:<span><?php print $fields['field_weight_value']->content; ?> кг</span>
				   </li>
			      <?php endif; ?>
			</ul>			
		    </div>
		    <div class="product-by">
			<div class="manufacturer"><h3><?php print $fields['field_manufacturer_value']->content; ?></h3></span></div>
			<div class="price">
			    <div class="last_price"><?php print $fields['cost']->content; ?></div>
			    <div class="new_price"><?php print $fields['sell_price']->content; ?></div>
			</div>
			<div class="stock">
			  <?php print $fields['field_stock_product_value']->content; ?>
			  <br><span>Срок поставки - <?php print $fields['field_delivery_value']->content; ?></span>
			</div>
			<?php print $fields['buyitnowbutton']->content; ?>
		    </div>
		</div>