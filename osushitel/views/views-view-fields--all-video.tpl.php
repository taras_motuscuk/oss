<?php if($fields['field_video_value']->content) { ?>
<div class="vid_inner">
		<div class="vid-inner-hover">
				<div class="vid_box">
						<a class="youtube colorbox-load" href="https://www.youtube.com/embed/<?php print $fields['field_video_value']->content; ?>?width=724&height=424&iframe=true&autoplay=1&rel=0&wmode=transparent" title="<?php print $fields['field_video_name_value']->content; ?><?php echo htmlspecialchars('<a href="'.$fields['path']->content.'" class="colorbox-button">Посмотреть осушитель</a>', ENT_QUOTES, "UTF-8"); ?>">
								<img class="group1 youtubeplay" src="https://img.youtube.com/vi/<?php print $fields['field_video_value']->content; ?>/0.jpg" alt="<?php print $fields['field_video_name_value']->content; ?>"/>				
								<div class="video-title"><?php print $fields['field_video_name_value']->content; ?></div>
						</a>
				</div>
				<div class="vid_desc">
					 <?php /*<div class="vid_p_title"><?php print $fields['title']->content; ?></div>*/?>
					 <div class="vid_p_text"><?php print $fields['body']->content; ?></div>
					 <?php /*<a href="<?php print $fields['path']->content; ?>">Подробнее о товаре</a>*/?>
				</div>
		</div>
</div>
 <?php } ?>