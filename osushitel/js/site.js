jQuery(function($){
  var SiteCheckWidth = $(document).width();
  $(window).resize(function() {
    SiteCheckWidth = $(document).width();  
  });  
  $(document).scroll(function() {
    $('.top-catalogue').hide();
    $('.top-catalogue').removeClass('fixed-top-catalogue');
    var y = $(this).scrollTop();
    if (y > 120) {
      $('.fixed-menu').fadeIn();
    } else {
      $('.fixed-menu').fadeOut();
    }
    if (SiteCheckWidth<969) {
      if($('body').hasClass('front')){
        var TopMobileCheck = 400;
      } else {
        var TopMobileCheck = 70;
      }
      if (y > TopMobileCheck) {
        $('.mobile-catalog-menu').addClass('mobile-fixed');
        $('.mobile-catalog-menu-links').addClass('mobile-fixed');
      } else {
        $('.mobile-catalog-menu').removeClass('mobile-fixed');
        $('.mobile-catalog-menu-links').removeClass('mobile-fixed');
      }
    }    
  });
  $(".tabs:not(.primary) li").on( "click", function() {
    var idx = $(this).index();
    $(this).parent().find('.active').removeClass('active');
    $(this).addClass('active');
    $(this).parent().parent().find('.tabs-content').find('.active').removeClass('active');
    $(this).parent().parent().find('.tabs-content > li').each(function(index, value) {
      if(index==idx) $(this).addClass('active');
    });    
    return false;
  });
  $(".fixed-menu-catalogue").on( "click", function() {
    if ($(this).hasClass('active-top-catalogue')) {
      $(this).removeClass('active-top-catalogue');
      $('.top-catalogue').hide();
      $('.top-catalogue').removeClass('fixed-top-catalogue');
    } else {
      $(this).addClass('active-top-catalogue');
      $('.top-catalogue').show();
      $('.top-catalogue').addClass('fixed-top-catalogue');
    }
    return false;
  });  
  $(".top-menu li.menu-path-productslist").on( "click", function() {
    $('.top-catalogue').removeClass('fixed-top-catalogue');
    if ($(this).hasClass('active-top-catalogue')) {
      $(this).removeClass('active-top-catalogue');
      $('.top-catalogue').hide();
    } else {
      $(this).addClass('active-top-catalogue');
      $('.top-catalogue').show();
    }
    return false;
  });
  $('html').on( "click", function(e) {
    if($(e.target).closest('.top-catalogue').length === 0){
      $('.top-catalogue').hide();
      $(".top-menu li.menu-path-productslist").removeClass('active-top-catalogue');
      $('.top-catalogue').removeClass('fixed-top-catalogue');
      $(".fixed-menu-catalogue").removeClass('active-top-catalogue');
    }
  });
  if ($('#ei-slider').length) {
    $('#ei-slider').eislideshow({
      speed            : 1000,
      animation : 'center',
      autoplay : true,
      slideshow_interval : 8000,
      titlesFactor : 0,
      titlespeed : 10,
    });
  }
});