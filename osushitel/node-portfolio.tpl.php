<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">
  <div class="global-region">
    <div class="node-tabs">
      <ul>
        <li><a href="/article">Блог</a></li>
        <li><a href="/news">Новости</a></li>
        <li><a href="/portfolio" class="active">Реализованные проекты</a></li>
      </ul>
      <div class="created"><?php echo format_date($node->created, $type = 'custom', 'd F Y'); ?></div>
    </div>
    <div class="node-content">
      <?php if(isset($node->field_news_image[0]['filepath'])): ?>
        <img src="/<?php echo $image_path = imagecache_create_path('product_list',$node->field_news_image[0]['filepath']); ?>" alt="<?php print $node->title; ?>" title="<?php print $node->title; ?>" class="node-image">
      <?php endif; ?>
      <div class="node-header">
        <h1 class="node-title"><?php print $title; ?></h1>
        <div class="social">
          <?php echo theme_social_share('googleplus', $node);
          echo theme_social_share('facebook', $node);
          ?></div>
        <div class="node-icons">          
          <div class="node-icon node-icon-user">Осушители</div>
          <div class="node-icon node-icon-views"><?php echo calculator_nodeview_count($node->nid); ?></div>
        </div>  
      </div>
      <div class="node-text">
       <?php echo CleanContent($content); ?>
      </div>
    </div>    
  </div>
</div>

