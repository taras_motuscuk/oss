<?php include('header.php'); ?>
  <div class="inner-blocks inner-blocks-brands">
    <div class="wrapper-inner">
      <div class="inner-region-left">
	<?php print $filter; ?>
      </div>
      <div class="inner-region-right">
	<h1 class="category-title">Осушители <?php echo $title; ?></h1>
	<div class="catalogue">
	  <?php BuildCatalog(); ?>
	</div>
      </div>
    </div>
  </div>			
  <div class="articles-blocks-bottom">
    <div class="wrapper-inner">
      <?php echo views_embed_view('article_block_4','default');?>
    </div>
  </div>
  <div class="top-products-info top-products-info-brand">
    <div class="wrapper-inner">
      <div class="top-products-info-text">
	<h1><?php echo $title; ?></h1>
	<?php
	  $term = taxonomy_get_term(arg(2));
	  $fields = term_fields_get_fields_values($term);
	  if(isset($fields['brand_text_value']) && $fields['brand_text_value']) echo $fields['brand_text_value'];
	?>
      </div>
    </div>
  </div>  
<?php include('footer.php'); ?>
