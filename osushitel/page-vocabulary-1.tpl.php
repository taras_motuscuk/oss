<?php include('header.php'); ?>
  <?php /*
  <div class="top-products-info">
    <div class="wrapper-inner">
      <div class="top-products-info-menu">
	<ul>
	  <?php
	    $active = '';
	    $tree = taxonomy_get_tree(7,0,-1);
	    foreach($tree as $key => $term) if(arg(2)==$term->tid) $active =' active-category';
	    foreach($tree as $key => $term) {		
	      if($key) $class="info-menu-sub-menu"; else $class="info-menu-top-menu";
	      echo '<li class="'.$class.$active.'">'.l($term->name, taxonomy_term_path($term)).'</li>';
	    }
	    $active = '';
	    $tree = taxonomy_get_tree(9,0,-1);
	    foreach($tree as $key => $term) if(arg(2)==$term->tid) $active =' active-category';
	    foreach($tree as $key => $term) {		
	      if($key) $class="info-menu-sub-menu"; else $class="info-menu-top-menu";
	      echo '<li class="'.$class.$active.'">'.l($term->name, taxonomy_term_path($term)).'</li>';
	    }
	    $active = '';
	    $tree = taxonomy_get_tree(11,0,-1);
	    foreach($tree as $key => $term) if(arg(2)==$term->tid) $active =' active-category';
	    foreach($tree as $key => $term) {		
	      if($key) $class="info-menu-sub-menu"; else $class="info-menu-top-menu";
	      echo '<li class="'.$class.$active.'">'.l($term->name, taxonomy_term_path($term)).'</li>';
	    }
	  ?>
	<ul>
      </div>
      <div class="top-products-info-text">

      </div>
    </div>
  </div>
  */?>
  <div class="top-products-blocks">
    <div class="wrapper-inner">
      <h2>Топ продаж</h2>
      <?php $term = taxonomy_get_term(arg(2)); ?>
      <?php echo views_embed_view('top3','default',$term->tid);?>
    </div>
  </div>			
  <div class="inner-blocks">    
    <div class="wrapper-inner">
      <div class="inner-region-left">
	<?php print $filter; ?>
      </div>
      <div class="inner-region-right">
	<h1 class="category-title"><?php echo calculator_h1_change($title); ?></h1>
	<div class="catalogue">
	  <?php BuildCatalog(); ?>
	</div>
      </div>
    </div>
  </div>
  <div class="articles-blocks-bottom">
    <div class="wrapper-inner">
      <?php echo views_embed_view('article_block_4','default');?>
    </div>
  </div>
  <?php /*
  <div class="seo-wrapper">
    <div class="wrapper-inner">
      <?php print $seobytovye; ?>
      <?php print $seobasseinov; ?>
      <?php print $seopromyshlennye; ?>
      <?php print $seoproizvoditeli; ?>
    </div>
  </div>
  */?>
  <div class="top-products-info top-products-info-brand">
    <div class="wrapper-inner">
      <div class="top-products-info-text">
	<?php /*<h1><?php echo $title; ?></h1>*/?>
  	<?php
	  $term = taxonomy_get_term(arg(2));
	  $fields = term_fields_get_fields_values($term);
	  if(isset($fields['category_description_value']) && $fields['category_description_value']) echo $fields['category_description_value'];
	  if(isset($fields['category_description_bas_value']) && $fields['category_description_bas_value']) echo $fields['category_description_bas_value'];
	  if(isset($fields['category_description_prom_value']) && $fields['category_description_prom_value']) echo $fields['category_description_prom_value'];
	?>
      </div>
    </div>
  </div>  	
<?php include('footer.php'); ?>
