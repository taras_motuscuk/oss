<?php include('header.php'); ?>
  <div class="inner-blocks inner-blocks-notfound">
    <div class="wrapper-inner">
      <h1>ПРОМАХ!</h1>
      <p>Вы попали на страницу, которая вроде как бы есть, а вроде как бы её и нет...</p>
      <div class="not-found-block">
	<div class="not-found-block-inner">
	  <p><b>Но мы Вас выведем куда нужно! Следуйте по ссылкам:</b></p>
	  <div class="not-found-block-item">
	    <a href="/productslist">Каталог осушителей воздуха</a> <span>- самый широкий каталог осушителей<br>&nbsp;&nbsp;в украинском Интернете.</span>
	  </div>
	  <div class="not-found-block-item">
	    <a href="/article">Статьи на тему осушения воздуха</a> <span>- да! Это узкоспециализированные<br>&nbsp;&nbsp;статьи по контролю влажности.<br>&nbsp;&nbsp;Обязательно посмотрите!</span>
	  </div>
	</div>
      </div>
    </div>
  </div>			
<?php include('footer.php'); ?>
