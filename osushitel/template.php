<?php

function off_line_status($proceed=FALSE) {
    if(!$proceed) return;
    if(is_array($_SESSION['messages']['status'])) {
    $offline = l(t('Operating in off-line mode.'), 'admin/settings/site-maintenance');
    $statuses = array();
    foreach($_SESSION['messages']['status'] as $status) {
        if($status != $offline) $statuses[] = $status;
    }
    if(!count($statuses))    {
        unset($_SESSION['messages']['status']);
    } else {
        $_SESSION['messages']['status'] = $statuses;
    }
  }
}

function osushitel_preprocess_page(&$vars) {
  if(arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
    $term = taxonomy_get_term(arg(2));
    if ($term->vid != 10 && $term->vid != 13) {
      $vars['template_files'][] = 'page-vocabulary-1';
    }
    if ($term->vid == 13) {
      $vars['template_files'][] = 'page-vocabulary-2';
    }
    if ($term->vid == 10) {
      $vars['template_files'][] = 'page-vocabulary-3';
    }
  }
  if (strpos(drupal_get_headers(), '404 Not Found') !== FALSE) {
    $vars['template_files'][] = 'page-notfound';
  }
}
