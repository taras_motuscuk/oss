<?php include('header.php'); ?>
  <?php if($is_front) { ?>
    <div class="catalogue-blocks">
      <div class="wrapper-inner">
	<div class="catalogue-block first">
	    <?php
	      $tree = taxonomy_get_tree(7,0,-1);
	      foreach($tree as $key => $term) {				
		if(!$key) {
		  $link = '<div><a href="/bytovye">Смотреть все товары раздела</a></div>';
		  echo '<div><a href="/bytovye">Бытовые</a></div>';
		  echo '<img src="/sites/all/themes/osushitel/img/catalogue-block-1.png">';
		} else {
		  echo '<div>'.l($term->name, taxonomy_term_path($term)).'</div>';
		}
	      }
	      echo $link;
	    ?>
	</div>
	<div class="catalogue-block bg">
	    <?php
	      $tree = taxonomy_get_tree(9,0,-1);
	      foreach($tree as $key => $term) {				
		if(!$key) {
		  $link = '<div><a href="/dlya-basseinov">Смотреть все товары раздела</a></div>';
		  echo '<div><a href="/dlya-basseinov">Для бассейнов</a></div>';
		  echo '<img src="/sites/all/themes/osushitel/img/catalogue-block-2.png">';
		} else {
		  echo '<div>'.l($term->name, taxonomy_term_path($term)).'</div>';
		}
	      }
	      echo $link;
	    ?>
	</div>
	<div class="catalogue-block">
	    <?php
	      $tree = taxonomy_get_tree(11,0,-1);
	      foreach($tree as $key => $term) {		
		echo '<div>'.l($term->name, taxonomy_term_path($term)).'</div>';
		if(!$key) {
		  $link = '<div>'.l('Смотреть все товары раздела', taxonomy_term_path($term)).'</div>';
		  echo '<img src="/sites/all/themes/osushitel/img/catalogue-block-3.png">';
		}
	      }
	      echo $link;
	    ?>
	</div>
	<div class="catalogue-block bg last">
	    <?php
	      $tree = taxonomy_get_tree(10,0,-1);
	      foreach($tree as $key => $term) {
		if(!$key) {
		  $link = '<div>'.l('Смотреть все бренды', taxonomy_term_path($term)).'</div>';
		}		
		if($key<9) echo '<div>'.l($term->name, taxonomy_term_path($term)).'</div>';
	      }
	      echo '<div>...</div>';
	      echo $link;
	    ?>
	</div>	
      </div>
    </div>
    <div class="products-blocks">
      <div class="wrapper-inner">
	  <ul class="tabs">
	    <li class="active"><a href="#">Акции</a></li>
	    <li><a href="#">Топ продаж</a></li>
	  </ul>
	  <ul class="tabs-content">
	    <li class="active"><?php echo views_embed_view('product_frontpage','default');?></li>
	    <li><?php echo views_embed_view('product_popular','default');?></li>
	  </ul>
      </div>
    </div>
    <div class="video-blocks">
      <div class="wrapper-inner">
	<a href="/video" class="btn big-btn">Все обзоры</a>
	<?php echo views_embed_view('front_video','default');?>
      </div>
    </div>
    <div class="ask-news-blocks">
      <div class="wrapper-inner">
	<div class="ask-block">
	  <?php print $askblock; ?>
	</div>
	<div class="news-articles-block">
	  <ul class="tabs">
	    <li class="active"><a href="#">Новости</a></li>
	    <li><a href="#">Статьи</a></li>
	  </ul>
	  <ul class="tabs-content">
	    <li class="active">
	      <a href="/news" class="tabs-link">Смотреть все новости</a>
	      <?php echo views_embed_view('news_block','default');?>
	    </li>
	    <li><?php echo views_embed_view('article_block','default');?></li>
	  </ul>
	</div>
      </div>
    </div>
    <div class="question-seo-blocks">
      <div class="wrapper-inner">
	<div class="question-block">
	  <?php print $poll; ?>
	</div>
	<div class="seo-block">    
	  <?php echo views_embed_view('seo_text','default');?>
	</div>
      </div>
    </div>	  
  <?php } else { ?>
    <div class="inner-blocks">
      <div class="wrapper-inner">		
	<?php /*<h1><?php print $title; ?></h1>*/?>
	<?php print $messages; ?>
	<div class="wrapper-inner-tabs"><?php print $tabs; ?></div>
	<div class="content-inner-wrapper">
	  <?php if(arg(0)=='ask' && arg(1)=='' ){ ?>
	    <div class="qestions-page-left">	      
	      <div class="qestions-img-box">
		  <?php print $fotoingener; ?>
	      </div>
	      <div class="qestions-info-text">
		<?php print $textask; ?>
	      </div>
	      <div class="qestions-ask-form">
		<h2>Задать вопрос специалисту</h2>
		<?php echo drupal_get_form ('virdini_ask_application_form') ?>
	      </div>	      	      
	    </div>
	  <?php } ?>
	  <?php if(arg(0)=='contacts' && arg(1)=='' ){ ?>
	    <div class="page-contacts-inner">
	      <div class="contacts">
		<?php print $contacts; ?>
	      </div>
	      <div class="map">
		<script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=r3HlCS7V3BEdL_WDNNCSJ_EgMuiogHrC&width=305&height=250"></script>
	      </div>
	      <div class="contacts-form">
		<?php echo drupal_get_form ('virdini_contact_application_form') ?>
	      </div>
	    </div>
	  <?php } ?>
	  <?php if($_REQUEST['name']) echo '<h3 class="calc-title" style="font-size:18px;">Проверяем, подходит ли осушитель '.$_REQUEST['name'].' для следующей задачи:</div>'; ?>
	  <div class="content-inner">
	    <?php print $content; ?>
	    <?php if(arg(0)=='call' ){ ?>
	      <?php echo drupal_get_form ('virdini_manager_application_form') ?>
	    <?php } ?>
	  </div>
	  <?php /*
	  <div style="display: none;overflow-x: hidden;">
	    <div id="checkout">
	      <div class="title-block">Оформить заказ</div>
		<?php
		  module_load_include('module', 'uc_cart', 'uc_cart_checkout_form');
		  echo drupal_get_form('uc_cart_checkout_form');
		?>
	      </div>
	    </div>
	  </div>
	  */?>
	</div>
      </div>
    </div>
    <?php if(arg(0)=='node' && is_numeric(arg(1))): ?>
      <?php $type = db_result(db_query("SELECT type FROM {node} WHERE nid = ".arg(1))); ?>
      <?php if($type=='product'): ?>
      <div class="top-products-blocks">
	<div class="wrapper-inner">
	  <h2>Похожие товары</h2>
	  <?php $term = taxonomy_get_term(arg(2)); ?>
	  <?php echo views_embed_view('top3','default',$term->tid);?>
	</div>
      </div>
      <?php endif; ?>
    <?php endif; ?>
  <?php } ?>
<?php include('footer.php'); ?>
