<div id="node-<?php print $node->nid; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?>">

  <div class="node-header">

    <?php if (!$page): ?>
      <h2 class="node-title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>



  </div>

  <div class="content">
    <?php print $picture ?>
    <?php print $content ?>
  </div>

</div>

